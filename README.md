# Schoolcraftteam Utils

## MySQL 2 PDO

This package implements global functions which can be used as replacement for deprecated [MySQL Functions](https://www.php.net/manual/en/ref.mysql.php).

The global functions mimic the mysql functions using [PDO](https://www.php.net/manual/en/book.pdo.php) with prepared statements. Each mysql function (for example: `mysql_connect`) can be replaced with a similar function from this package with `pdo_` prefix, for example `pdo_mysql_connect`.

This way, the refactoring effort for old code using mysql functions is held at minimum effort, since only function names have to be replaced.

Sometimes, additional parameters have to be handed over to the replacement functions, so that the proper replacements can be made with prepared statements.

Example:

```
mysql_query('STATEMENT WITH ' . $variable . ', $dbLink);
```

```
pdo_mysql_query('STATEMENT WITH :variable', $dbLink, [
    ':variable' => $variable
]);
```
