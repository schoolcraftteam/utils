<?php

/////////////////
// This file defines global PHP functions which can be used in PHP >= 7.x projects
// in order to replace old fashioned mysql_* functions with PDO prepared statements functionality.
//
// The aim of these replacement functions is to provide an easy way for refactoring deprecated
// functions with minimum effort.
//
// The replacement functions given in this package are prefixed with pdo_ in order to aviod
// conflicts with existing mysql_ functions.
//
// To refactor old fashioned mysql_ functions in your project, just prefix those functions
// with pdo_ in your code and add an array of replacement parameters as third argument of
// pdo_mysql_query(), which must be used in order to implement prepared statements.
//
// Prerequisites:
// - PHP version 7 or higher
// - schoolcraftteam/pdohelper composer package
// - database credentials as enviroment variables (see pdohelper package)
/////////////////

require_once __DIR__ . '/PdoHelper.php';

use Schoolcraftteam\PdoHelper;

/**
 * Replacement for mysql_connect()
 * Requires environment variables
 *
 * @return mixed
 */
function pdo_mysql_connect($dbServer = null, $dbUser = null, $dbPw = null, $dbName = null) {

    $dbDriver = PdoHelper::getDatabaseDriver();
    $dbServer = $dbServer ?? PdoHelper::getDatabaseServer();
    $dbUser = $dbUser ?? PdoHelper::getDatabaseUsername();
    $dbPw = $dbPw ?? PdoHelper::getDatabasePassword();
    $dbName = $dbName ?? PdoHelper::getDatabaseName();
    $charset = $charset ?? PdoHelper::getDatabaseCharset();

    try {
        switch ($dbDriver) {
            case 'sqlite':
                $dbLink = new PDO('sqlite:' . $_ENV['DATABASE_DBNAME']);
                break;
            case 'mysql':
                $dbLink = new PDO('mysql:host=' . $dbServer . ';dbname=' . $dbName . ';' . $charset, $dbUser, $dbPw);
                break;
            default:
                throw new Exception('Unknown database driver');
        }
    } catch (PDOException $e) {
        if (isset($_ENV['ENVIRONMENT']) && in_array($_ENV['ENVIRONMENT'], ['develop', 'dev', 'staging', 'testing', 'test', 'local'])) {
            var_dump($e);
        }
        return false;
    }
    return $dbLink;
}

/**
 * Replacement for mysqli_connect
 *
 * @param [type] $dbServer
 * @param [type] $dbUser
 * @param [type] $dbPw
 * @param [type] $dbName
 * @return void
 */
function pdo_mysqli_connect($dbServer = null, $dbUser = null, $dbPw = null, $dbName = null) {
    return pdo_mysql_connect($dbServer, $dbUser, $dbPw, $dbName);
}

/**
 * Replacement for mysql_error()
 * Uses PdoHelper static var
 *
 * @return string
 */
function pdo_mysql_error() {
    return PdoHelper::getErrorInfo();
}

/**
 * Replacement for mysqli_error
 *
 * @param [type] $dbLink
 * @return string
 */
function pdo_mysqli_error($dbLink = null) {
    return pdo_mysql_error();
}

/**
 * Replacement for mysql_close()
 * Does nothing
 *
 * @param PDO $dbLink
 * @return null
 */
function pdo_mysql_close(PDO $dbLink = null) {
    return null;
}

/**
 * Replacement for mysqli_close
 *
 * @param PDO $dbLink
 * @return void
 */
function pdo_mysqli_close(PDO $dbLink = null) {
    return pdo_mysql_close($dbLink);
}

/**
 * Replacement for mysql_select_db()
 * Returns a new PDO object with selected database
 *
 * @param [type] $dbName
 * @param [type] $dbLink
 * @return PDO
 */
function pdo_mysql_select_db($dbName = null, PDO $dbLink = null) {
    return pdo_mysql_connect(null, null, null, $dbName);
}

/**
 * Replacement for mysqli_select_db
 *
 * @param PDO $dbLink
 * @param [type] $dbName
 * @return false|PDO|void
 */
function pdo_mysqli_select_db(PDO $dbLink = null, $dbName = null) {
    return pdo_mysql_select_db($dbName, $dbLink);
}

/**
 * Replacement for mysql_query()
 *
 * @param string $sql                 Query String
 * @param mixed $dbLink               Will not be used in this function, since it builds an own PDO connection with other replacement function
 * @param array $preparedArguments    New parameter used in refactoring: arguments used fo prepared statements
 * @return PDOStatement               Returns PDO statement
 */
function pdo_mysql_query($sql, $dbLink = null, $preparedArguments = []) {

    // preform db connection, instantiate PDO object
    $dbLink = isset($dbLink) && $dbLink ? $dbLink : pdo_mysql_connect();

    /////
    // PDO::ATTR_EMULATE_PREPARES Enables or disables emulation of prepared statements.
    // Some drivers do not support native prepared statements or have limited support for them.
    // Use this setting to force PDO to either always emulate prepared statements
    // (if true and emulated prepares are supported by the driver), or to try to use native prepared statements (if false).
    // It will always fall back to emulating the prepared statement if the driver
    // cannot successfully prepare the current query. Requires bool.
    /////
    // We need this in order to allow integer values for prepared statement replacements.
    // For example, a statement with "... LIMIT :start, :end" with integer $preparedArguments
    // will parse them as strings by default: "... LIMIT '5', '10'" and the query will fail.
    // Setting this attribute to false will prepare the statment with proper integer values,
    // for example "... LIMIT 5, 10".
    /////
    // @see https://www.php.net/manual/en/pdo.setattribute.php
    $dbLink->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    $dbLink->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // prepare given statement
    $sth = $dbLink->prepare($sql);

    // if statement preparation returns bool false, probably
    // the above set attribute caused a query error. In this case
    // fall back to default pdo 'emulate prepares' behaviour.
    if (!$sth) {
        $dbLink->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
        $sth = $dbLink->prepare($sql);
    }

    // execute given statement and replace arguments
    $exec = $sth->execute($preparedArguments);

    if (!$exec) {

        // set error info as object property of PdoHelper
        // in order to get it in pdo_mysql_error()
        // as simulation of mysql_error()
        PdoHelper::setErrorInfo($sth->errorInfo());

        if (isset($_ENV['SQLDEBUG']) && filter_var($_ENV['SQLDEBUG'], FILTER_VALIDATE_BOOLEAN) !== false) {
            $sth->debugDumpParams();
        }
        return false;
    }

    // set last insert id as object property of PdoHelper
    // in order to get it in pdo_mysql_insert_id()
    // as simulation of mysql_insert_id()
    PdoHelper::setLastInsertId($dbLink->lastInsertId());

    return $sth;
}

/**
 * Replacement for mysqli_query
 *
 * @param [type] $dbLink
 * @param [type] $sql
 * @param array $preparedArguments
 * @return false|PDOStatement|void
 */
function pdo_mysqli_query($dbLink = null, $sql = '', $preparedArguments = []) {
    return pdo_mysql_query($sql, $dbLink, $preparedArguments);
}

/**
 * Replacement for mysql_insert_id()
 *
 * @return int
 */
function pdo_mysql_insert_id() {
    // get static lastInsertId of helper class
    return PdoHelper::getLastInsertId();
}

/**
 * Replacement for mysqli_insert_id
 *
 * @return int
 */
function pdo_mysqli_insert_id() {
    return pdo_mysql_insert_id();
}

/**
 * Replacement for mysql_num_rows()
 *
 * @param PDOStatement $sth     PDOStatment object as result of previous pdo_mysql_query()
 * @return int
 */
function pdo_mysql_num_rows(PDOStatement $sth) {
    return $sth->rowCount();
}

/**
 * Replacement for mysqli_num_rows
 *
 * @param PDOStatement $sth
 * @return int
 */
function pdo_mysqli_num_rows(PDOStatement $sth) {
    return pdo_mysql_num_rows($sth);
}

/**
 * Replacement for mysql_fetch_array()
 *
 * @param PDOStatement $sth     PDOStatment object as result of previous pdo_mysql_query()
 * @return mixed
 */
function pdo_mysql_fetch_array(PDOStatement $sth) {
    return $sth->fetch();
}

/**
 * Replacement for mysqli_fetch_array
 *
 * @param PDOStatement $sth
 * @return void
 */
function pdo_mysqli_fetch_array(PDOStatement $sth) {
    return pdo_mysql_fetch_array($sth);
}

/**
 * Replacement for mysql_fetch_assoc()
 *
 * @param PDOStatement $sth     PDOStatment object as result of previous pdo_mysql_query()
 * @return mixed
 */
function pdo_mysql_fetch_assoc(PDOStatement $sth) {
    return $sth->fetch(PDO::FETCH_ASSOC);
}

/**
 * Replacement for mysqli_fetch_assoc
 *
 * @param PDOStatement $sth
 * @return void
 */
function pdo_mysqli_fetch_assoc(PDOStatement $sth) {
    return pdo_mysql_fetch_assoc($sth);
}

/**
 * Replacement for mysql_fetch_object()
 *
 * @param PDOStatement $sth     PDOStatment object as result of previous pdo_mysql_query()
 * @return mixed
 */
function pdo_mysql_fetch_object(PDOStatement $sth) {
    return $sth->fetch(PDO::FETCH_OBJ);
}

/**
 * Replacement for mysqli_fetch_object
 *
 * @param PDOStatement $sth
 * @return void
 */
function pdo_mysqli_fetch_object(PDOStatement $sth) {
    return pdo_mysql_fetch_object($sth);
}

/**
 * Replacement for mysql_real_escape_string()
 * Just returns given string, since escaping should be done via prepared statements
 *
 * @param string $string
 * @return string
 */
function pdo_mysql_real_escape_string($string = null) {
    return $string;
}

/**
 * Replacement for mysqli_real_escape_string
 *
 * @param [type] $dbLink
 * @param [type] $string
 * @return string
 */
function pdo_mysqli_real_escape_string($dbLink = null, $string = null) {
    return pdo_mysql_real_escape_string($string);
}

/**
 * Replacement for mysql_free_result
 *
 * @param mixed $arg
 * @return void
 */
function pdo_mysql_free_result($arg = null) {
    // stub function, only needs to exist. PDO handles freeing it self
}

/**
 * Replacement for mysqli_free_result
 *
 * @param [type] $arg
 * @return void
 */
function pdo_mysqli_free_result($arg = null) {
    pdo_mysql_free_result($arg);
}

/**
 * noop replacement for mysql_set_charset
 * pdo_mysql_connect uses charset from .env or defaults to UTF-8
 *
 * @param  string  $charset
 * @param  null  $dbLink
 */
function pdo_mysql_set_charset($charset = "UTF8", $dbLink = null)
{

}

/**
 * Replacement for mysql_fetch_row
 *
 * @param  PDOStatement  $result
 * @return mixed
 */
function pdo_mysql_fetch_row(PDOStatement $result)
{
    return $result->fetch();
}

/**
 * Replacement for mysqli_fetch_row
 * @param  PDOStatement  $result
 * @return mixed
 */
function pdo_mysqli_fetch_row(PDOStatement $result)
{
    return $result->fetch();
}
