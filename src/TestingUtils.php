<?php

namespace Schoolcraftteam;

use Schoolcraftteam\PdoHelper;
use \PDO;
use \PDOException;

class TestingUtils
{
    //=============================================================================
    // Helper Functions to execute curl calls
    //=============================================================================

    public static function createCurl($baseUrl, $requestType, $parameter = array())
    {
        $curl = curl_init();

        $url = $baseUrl;
        if($requestType == 'GET' &&  !empty($parameter) )
        {
            $getParams = http_build_query($parameter);
            $url = $baseUrl."?".$getParams;
        }

        curl_setopt_array( $curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => $requestType,
            CURLOPT_POSTFIELDS => $parameter,
        ));

        return $curl;
    }

    public static function execCurlCall($curl)
    {
        $response = curl_exec( $curl );

        if ( curl_errno( $curl ) ) {
            error_log( 'Error in curl call. Response: ' . $response );
            error_log( 'Curl error message output: ' . curl_error( $curl ) );
        }

        curl_close( $curl );

        return $response;
    }


    //=============================================================================
    // Misc Helper Functions
    //=============================================================================

    public static function dbLink()
    {
        return pdo_mysql_connect(PdoHelper::getDatabaseServer(), PdoHelper::getDatabaseUsername(), PdoHelper::getDatabasePassword(), PdoHelper::getDatabaseName());
    }

    public static function startsWith( $haystack, $needle )
    {
        $length = strlen( $needle );
        return substr( $haystack, 0, $length ) === $needle;
    }

    public static function endsWith( $haystack, $needle )
    {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }

    public static function randomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-/(';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

?>