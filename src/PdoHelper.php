<?php

namespace Schoolcraftteam;

/**
 * Helper class to emulate last insert id.
 * Setter should be called on every query.
 * Getter will return static class var.
 */
class PdoHelper
{

    /**
     * statically store last insert id
     *
     * @var integer
     */
    private static $lastInsertId = 0;

    /**
     * statically store error info
     */
    private static $errorInfo = null;

    public static function getDatabaseDriver()
    {
        return $_ENV['DATABASE_DRIVER'] ?? 'mysql';
    }

    /**
     * Getter for DB username
     *
     * @return string
     */
    public static function getDatabaseUsername()
    {
        return $_ENV['DATABASE_USER'] ??
            $_ENV['DATABASE_USERNAME'] ??
            $_ENV['DB_USER'] ??
            $_ENV['DB_USERNAME'] ?? null;
    }

    /**
     * Getter for DB password
     *
     * @return string
     */
    public static function getDatabasePassword()
    {
        return $_ENV['DATABASE_PASSWORD'] ??
            $_ENV['DATABASE_PW'] ??
            $_ENV['DB_PASSWORD'] ??
            $_ENV['DB_PW'] ?? null;
    }

    /**
     * Getter for DB servername
     *
     * @return string
     */
    public static function getDatabaseServer()
    {
        return $_ENV['DATABASE_SERVER'] ??
            $_ENV['DATABASE_HOST'] ??
            $_ENV['DB_HOST'] ??
            $_ENV['DB_SERVER'] ?? null;
    }

    /**
     * Getter for DB name
     *
     * @return string
     */
    public static function getDatabaseName()
    {
        return $_ENV['DATABASE_DBNAME'] ??
            $_ENV['DATABASE_NAME'] ??
            $_ENV['DATABASE_DBNAME'] ??
            $_ENV['DATABASE_DB'] ??
            $_ENV['DB_DBNAME'] ??
            $_ENV['DB_NAME'] ??
            $_ENV['DB_DBNAME'] ??
            $_ENV['DB_DATABASE'] ?? null;
    }

    /**
     * Getter for charset
     *
     * @return string
     */
    public static function getDatabaseCharset()
    {
        return $_ENV['DATABASE_CHARSET'] ??
            $_ENV['DB_CHARSET'] ??
            'utf8';
    }

    /**
     * statically set last insert id var
     *
     * @param integer $id
     * @return void
     */
    public static function setLastInsertId($id = 0)
    {
        self::$lastInsertId = $id ? $id : 0;
    }

    /**
     * statically get last insert id
     *
     * @return int
     */
    public static function getLastInsertId()
    {
        return self::$lastInsertId;
    }

    /**
     * statically set error info
     *
     * @param integer $id
     * @return void
     */
    public static function setErrorInfo($info = null)
    {
        self::$errorInfo = $info ? $info : null;
    }

    /**
     * statically get error info
     *
     * @return void
     */
    public static function getErrorInfo()
    {
        return var_dump(self::$errorInfo);
    }

}
